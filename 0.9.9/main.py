from gtts import gTTS
import os
import get_holidays

mytext = 'Итак, праздники на сегодня \n'
mytext += get_holidays.getHoliday()

#mytext = 'Оля, я тебя люблю'

# Language in which you want to convert
language = 'ru'

# Passing the text and language to the engine,
# here we have marked slow=False. Which tells
# the module that the converted audio should
# have a high speed
myobj = gTTS(text=mytext, lang=language, slow=False)

# Saving the converted audio in a mp3 file named
# welcome
myobj.save("welcome.mp3")

# Playing the converted file
os.system("welcome.mp3")