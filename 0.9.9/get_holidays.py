# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup

engAlph = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'


# получение всех праздников, возвращает строку
def getHoliday():
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36'}
    req = requests.get('https://kakoysegodnyaprazdnik.ru/', headers=headers)
    #меняем кодировку текста в аппартаную, чтобы читать кириллицу (windows-125хз попробовать?)
    req.encoding = req.apparent_encoding
    # print(req.text)
    soup = BeautifulSoup(req.text, "html.parser")
    rezs = soup.find_all('span', itemprop="text")
    textholidays = ""
    i = 0
    for i in range(0, 11):
        textholidays += rezs[i].text
        textholidays += '.\n'
    # убираем английские буковы, чтобы праздники читались только на русском
    # плюс tts криво читает english text когда выставлен на русский язык
    textholidays = textholidays.translate({ord(i): None for i in engAlph})
    return textholidays


if __name__ == "__main__":
    tmp = getHoliday()
    print(tmp)
