import pyttsx3
import get_holidays

tts = pyttsx3.init()
voices = tts.getProperty('voices')
tts.setProperty('voice','ru')
tts.setProperty('rate', 185)

for voice in voices:
    if voice.name == 'Mikhail':
        tts.setProperty('voice', voice.id)

texttosay = 'Итак, праздники на сегодня. \n'
texttosay += get_holidays.getHoliday()

tts.say(texttosay) #вывод в mp3?
tts.runAndWait()
tts.stop()